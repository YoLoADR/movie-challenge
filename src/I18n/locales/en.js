export default {
  common: {
    noInternet: 'No Internet Connection',
  },
  welcome: {
    moviePlayer: 'Movie Player',
    searchMovie: 'Search Movie/Show',
  },

  detail: {
    rating: 'Rating :',
  },
};
