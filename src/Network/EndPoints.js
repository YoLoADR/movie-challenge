import APPConfig from '../Helper/APPConfig';

const EndPoints = {
  getMovieList: `${APPConfig.API_URL}`,
};

export default EndPoints;
