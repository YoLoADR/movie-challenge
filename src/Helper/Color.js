const Color = {
  white: '#ffffff',
  black: '#000000',

  transparent: 'transparent',
  whiteShadeFA: '#FAFAFA',
};

export default Color;
