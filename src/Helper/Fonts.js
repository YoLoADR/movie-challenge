const Fonts = {
  OpenSans_Bold: 'OpenSans-Bold',
  OpenSans_Light: 'OpenSans-Light',
  OpenSans_Medium: 'OpenSans-Medium',
  OpenSans_Regular: 'OpenSans-Regular',
  OpenSans_SemiBold: 'OpenSans-SemiBold',
};

export default Fonts;
