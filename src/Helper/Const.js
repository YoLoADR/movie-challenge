import I18n from '../I18n/i18n';

const Const = {
  lang: I18n,
  app_store_link: 'https://apps.apple.com/app/',
  play_store_link: 'https://play.google.com/store/apps/details?id=',
  emitEvent: {
    SELECT_WORKOUT: 'SELECT_WORKOUT',
  },
};

export default Const;
