const isSTAGING = false;

const APPConfig = {
  API_URL: 'https://api.tvmaze.com/shows',
};

export default APPConfig;
